package br.com.nicetravel.model.enuns;


public enum StyleActivity {

    RESTAURANT( "Restaurant"),
    BAR( "Bar"),
    MUSEUM("Museum"),
    SHOP( "Shop"),
    HISTORICAL_MONUMENT( "Historical monument"),
    SWIMMING( "Swimming"),
    PARK( "Park"),
    CHURCH( "Church"),
    SPORT( "Sport"),
    OTHER( "Other"),
    BREAK_FAST( "Break Fast"),
    PARTY( "Party"),
    BEACH( "Beach"),
    FITNESS( "Fitness"),
    SHOWER( "Shower"),
    BED( "Bed");

    public static final String CLASS_NAME = "br.com.nicetravel.model.enuns.StyleActivity";

    private final String description;

    StyleActivity(String description) {
        this.description = description;
    }

    public static StyleActivity valueOfEnum(String description) {
        for (StyleActivity type : StyleActivity.values()) {
            if (description.trim().equalsIgnoreCase(type.getDescription())) {
                return type;
            }
        }
        return null;
    }

    public String getDescription() {
        return description;
    }

}
