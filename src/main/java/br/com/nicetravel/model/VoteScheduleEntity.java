package br.com.nicetravel.model;


import br.com.nicetravel.util.Constants;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "TB_VOTE_SCHEDULE", schema = Constants.SCHEMA)
public class VoteScheduleEntity extends BaseEntity {

    private static final String PK_GENERATOR_NAME = "PK_VOTE_SCHEDULE";

    @Id
    @Column(name = "CO_VOTE_SCHEDULE")
    @GeneratedValue(generator = PK_GENERATOR_NAME, strategy = GenerationType.AUTO)
    private Long cod;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CO_USER", foreignKey = @ForeignKey(name = "FK_VOTE_SCHEDULE_TO_USER"), nullable = false)
    private UserEntity userVote;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CO_SCHEDULE_TRAVEL", foreignKey = @ForeignKey(name = "FK_VOTE_SCHEDULE_TO_SCHEDULE_TRAVEL"), nullable = false)
    private ScheduleTravelEntity scheduleTravelEntity;

    @Column(name = "ST_LIKE", nullable = false)
    private Boolean like;

    @Column(name = "NU_STAR") /*0-5*/
    private Double qtdStart;

    @Column(name = "DS_COMMENTARY", length = 1000)
    private String commentary;

    @Column(name = "DT_VOTE", nullable = true)
    private LocalDate dtVote;

    @Override
    public Long getCod() {
        return cod;
    }

    public void setCod(Long cod) {
        this.cod = cod;
    }

    public UserEntity getUserVote() {
        return userVote;
    }

    public VoteScheduleEntity setUserVote(UserEntity userVote) {
        this.userVote = userVote;
        return this;
    }

    public ScheduleTravelEntity getScheduleTravelEntity() {
        return scheduleTravelEntity;
    }

    public VoteScheduleEntity setScheduleTravelEntity(ScheduleTravelEntity scheduleTravelEntity) {
        this.scheduleTravelEntity = scheduleTravelEntity;
        return this;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(Boolean liked) {
        this.like = liked;
    }

    public Double getQtdStart() {
        return qtdStart;
    }

    public void setQtdStart(Double photoLink) {
        this.qtdStart = photoLink;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public LocalDate getDtVote() {
        return dtVote;
    }

    public VoteScheduleEntity setDtVote(LocalDate dtVote) {
        this.dtVote = dtVote;
        return this;
    }
}
