package br.com.nicetravel.model;

import br.com.nicetravel.util.Constants;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "TB_SCHEDULE_TRAVEL", schema = Constants.SCHEMA,
        indexes = { @Index(name = "IDX_DT_EXCLUSION", columnList = "DT_EXCLUSION")})
public class ScheduleTravelEntity extends BaseEntity implements DuplicateEntity<UserEntity> {

    private static final String PK_GENERATOR_NAME = "PK_SCHEDULE_TRAVEL";

    @Id
    @Column(name = "CO_SCHEDULE_TRAVEL")
    @GeneratedValue(generator = PK_GENERATOR_NAME, strategy = GenerationType.AUTO)
    private Long cod;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "scheduleTravelEntity", cascade = CascadeType.ALL)
    private List<ScheduleDayEntity> scheduleDayEntities = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CO_CITY", foreignKey = @ForeignKey(name = "FK_SCHEDULE_TRAVEL_TO_CITY"), nullable = false)
    private CityEntity cityEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CO_USER", foreignKey = @ForeignKey(name = "FK_SCHEDULE_TRAVEL_TO_USER"), nullable = false)
    private UserEntity userOwner;

    @Column(name = "ST_PUBLIC_ACCESS", nullable = false)
    private Boolean publicAccess;

    @Column(name = "NU_LIKE", nullable = false)
    private Integer numberLike;

    @Column(name = "DT_EXCLUSION", nullable = true)
    private LocalDate exclusionDate;

    @PrePersist
    public void prePersist() {
        if (numberLike == null) {
            numberLike = 0;
        }
        if (publicAccess == null) {
            publicAccess = Boolean.FALSE;
        }
    }

    @Override
    public ScheduleTravelEntity duplicate(UserEntity userOwner) {
        ScheduleTravelEntity scheduleTravelEntity = new ScheduleTravelEntity();
        scheduleTravelEntity.setCityEntity(this.getCityEntity());
        scheduleTravelEntity.setUserOwner(userOwner);
        scheduleTravelEntity.setScheduleDayEntities(scheduleDayEntities
                .stream()
                .map(day -> day.duplicate(scheduleTravelEntity))
                .collect(Collectors.toList()));
        return scheduleTravelEntity;
    }

    @Override
    public Long getCod() {
        return cod;
    }

    public void setCod(Long cod) {
        this.cod = cod;
    }

    public List<ScheduleDayEntity> getScheduleDayEntities() {
        return scheduleDayEntities;
    }

    public void setScheduleDayEntities(List<ScheduleDayEntity> scheduleDayEntities) {
        this.scheduleDayEntities = scheduleDayEntities;
    }

    public CityEntity getCityEntity() {
        return cityEntity;
    }

    public void setCityEntity(CityEntity cityEntity) {
        this.cityEntity = cityEntity;
    }

    public Boolean getPublicAccess() {
        return publicAccess;
    }

    public void setPublicAccess(Boolean publicAccess) {
        this.publicAccess = publicAccess;
    }

    public Integer getNumberLike() {
        return numberLike;
    }

    public void setNumberLike(Integer numberLike) {
        this.numberLike = numberLike;
    }

    public List<String> getCityImageUrl(int maxSize) {
        return getCityImageUrl().stream().limit(maxSize).collect(Collectors.toList());
    }

    public List<String> getCityImageUrl() {
        return cityEntity.getPhotosLinks();
    }

    public String getFormattedAdress() {
        return cityEntity.getFormattedAddress();
    }

    public UserEntity getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(UserEntity userOwner) {
        this.userOwner = userOwner;
    }

    public LocalDate getExclusionDate() {
        return exclusionDate;
    }

    public void setExclusionDate(LocalDate exclusinDate) {
        this.exclusionDate = exclusinDate;
    }

    public BigDecimal getPriceFinal() {
        if (CollectionUtils.isEmpty(scheduleDayEntities)) {
            return BigDecimal.ZERO;
        }
        return scheduleDayEntities.stream()
                .filter(a -> a.getPriceDay() != null)
                .map(ScheduleDayEntity::getPriceDay)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
