package br.com.nicetravel.job;

import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.service.travel.persist.AbstractTravelScheduleService;
import io.quarkus.scheduler.Scheduled;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.List;

@ApplicationScoped
public class RemoveScheduleTravelJob {

    @Autowired
    private AbstractTravelScheduleService travelScheduleService;

    @Scheduled(cron="0 0 0 5,10,20 * ?")
    void deleteSchedules() {
        List<ScheduleTravelEntity> allRemovedSchedule = travelScheduleService.findAllRemovedSchedule(LocalDate.now());
        allRemovedSchedule
                .forEach(s -> travelScheduleService.delete(s.getCod()));
    }

}
