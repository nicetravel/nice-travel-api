package br.com.nicetravel.job;

import io.quarkus.scheduler.Scheduled;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;

@ApplicationScoped
public class HerokuKeepAliveJob {

    private static final Logger LOG = LoggerFactory.getLogger(HerokuKeepAliveJob.class);
    public static final String NICE_TRAVEL_API_URL = "https://nicetravel-api.herokuapp.com/swagger-ui/#/";

    @Scheduled(cron = "0 0/5 * * * ?")
    void keepAlive() {
        LOG.debug("Call Nice Travel API");
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

            HttpGet request = new HttpGet(NICE_TRAVEL_API_URL);
            request.addHeader("Accept", MediaType.APPLICATION_JSON_VALUE);

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                if (response.getStatusLine().getStatusCode() == 200) {
                    LOG.info("Nice Travel is running");
                }
            }
        } catch (IOException e) {
            LOG.error("Heroku is not running", e);
        }
    }


}
