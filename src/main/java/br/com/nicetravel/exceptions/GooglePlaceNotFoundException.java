package br.com.nicetravel.exceptions;

public class GooglePlaceNotFoundException extends IntegrationException {

    public GooglePlaceNotFoundException(String message) {
        super(message);
    }
}
