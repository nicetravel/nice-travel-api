package br.com.nicetravel.service.scheduleday.retrieve;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResponse;

import java.util.List;

public interface AbstractFindScheduleDayService {
    List<ScheduleDayResponse> getScheduleDays(Long scheduleId);
}
