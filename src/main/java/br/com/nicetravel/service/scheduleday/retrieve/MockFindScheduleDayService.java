package br.com.nicetravel.service.scheduleday.retrieve;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResponse;
import br.com.nicetravel.model.ActivityEntity;
import br.com.nicetravel.model.ScheduleDayEntity;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MockFindScheduleDayService implements AbstractFindScheduleDayService {

    @Override
    public List<ScheduleDayResponse> getScheduleDays(Long scheduleId) {
        List<ScheduleDayResponse> scheduleDayResponses = new ArrayList<>();
        for (int i = 1; i <= scheduleId; i++) {
            ScheduleDayEntity scheduleDayEntity = new ScheduleDayEntity();
            scheduleDayEntity.setDay(i);
            scheduleDayEntity.setCod((long) i);
            ActivityEntity activityEntity = new ActivityEntity();
            activityEntity.setPrice(BigDecimal.valueOf(new SecureRandom().nextInt(250) + 100.21));
            scheduleDayEntity.setActivities(Collections.singletonList(activityEntity));
            scheduleDayResponses.add(new ScheduleDayResponse(scheduleDayEntity));
    }
        return scheduleDayResponses;
    }


}
