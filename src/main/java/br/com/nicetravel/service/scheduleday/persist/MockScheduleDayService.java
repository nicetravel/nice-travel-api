package br.com.nicetravel.service.scheduleday.persist;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResponse;
import br.com.nicetravel.model.ScheduleDayEntity;

public class MockScheduleDayService extends AbstractScheduleDayService {

    @Override
    public boolean deleteById(Long scheduleDayId) {
        return false;
    }

    @Override
    public ScheduleDayEntity saveScheduleDay(Long scheduleId) {
        ScheduleDayEntity scheduleDayEntity = new ScheduleDayEntity();
        scheduleDayEntity.setDay(10);
        scheduleDayEntity.setCod(10L);
        return scheduleDayEntity;
    }


    @Override
    protected ScheduleDayResponse createScheduleDayDTO(ScheduleDayEntity scheduleDayEntity) {
        return new ScheduleDayResponse(scheduleDayEntity);
    }

    @Override
    public void reorder(Long scheduleDayIdFrom, Long scheduleDayIdTo) {
        //NOOP
    }
}
