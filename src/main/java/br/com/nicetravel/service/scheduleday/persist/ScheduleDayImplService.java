package br.com.nicetravel.service.scheduleday.persist;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResponse;
import br.com.nicetravel.model.ScheduleDayEntity;
import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.repository.ScheduleDayRepository;
import br.com.nicetravel.repository.ScheduleTravelRepository;

import java.util.List;
import java.util.Optional;

public class ScheduleDayImplService extends AbstractScheduleDayService {

    private final ScheduleDayRepository scheduleDayRepository;

    private final ScheduleTravelRepository scheduleRepository;

    public ScheduleDayImplService(ScheduleDayRepository scheduleDayRepository, ScheduleTravelRepository scheduleRepository) {
        this.scheduleDayRepository = scheduleDayRepository;
        this.scheduleRepository = scheduleRepository;
    }

    @Override
    public boolean deleteById(Long scheduleDayId) {
        Optional<ScheduleDayEntity> scheduleDay = scheduleDayRepository.findById(scheduleDayId);
        if(scheduleDay.isPresent()) {
            updateDays(scheduleDay.get());
            scheduleDayRepository.deleteById(scheduleDay.get().getCod());
            return true;
        }
        return false;
    }

    private void updateDays(ScheduleDayEntity scheduleDay) {
        List<ScheduleDayEntity> schedulesDays = scheduleDayRepository.findAllByScheduleTravelEntityCodOrderByDay(scheduleDay.getScheduleTravelEntity().getCod());

        schedulesDays
                .stream()
                .filter(d -> d.getDay() > scheduleDay.getDay())
                .forEach(d -> d.setDay(d.getDay() - 1));

        scheduleDayRepository.saveAll(schedulesDays);

    }

    @Override
    public ScheduleDayEntity saveScheduleDay(Long scheduleId) {
        ScheduleDayEntity scheduleDayEntity = new ScheduleDayEntity();
        ScheduleTravelEntity scheduleTravel = scheduleRepository.getOne(scheduleId);
        scheduleDayEntity.setScheduleTravelEntity(scheduleTravel);
        scheduleDayEntity.setDay(scheduleTravel.getScheduleDayEntities().size() + 1);
        return scheduleDayRepository.save(scheduleDayEntity);
    }

    @Override
    public ScheduleDayResponse createScheduleDayDTO(ScheduleDayEntity scheduleDay) {
        return new ScheduleDayResponse(scheduleDay);
    }

    @Override
    public void reorder(Long scheduleDayIdFrom, Long scheduleDayIdTo) {
        ScheduleDayEntity scheduleDayFrom = scheduleDayRepository.getOne(scheduleDayIdFrom);
        ScheduleDayEntity scheduleDayTo = scheduleDayRepository.getOne(scheduleDayIdTo);
        int dayTo = scheduleDayTo.getDay();
        int dayFrom = scheduleDayFrom.getDay();
        scheduleDayTo.setDay(dayFrom);
        scheduleDayRepository.save(scheduleDayTo);
        scheduleDayFrom.setDay(dayTo);
        scheduleDayRepository.save(scheduleDayFrom);
    }
}
