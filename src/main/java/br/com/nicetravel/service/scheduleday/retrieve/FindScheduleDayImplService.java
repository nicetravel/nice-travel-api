package br.com.nicetravel.service.scheduleday.retrieve;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResponse;
import br.com.nicetravel.model.ScheduleDayEntity;
import br.com.nicetravel.repository.ScheduleDayRepository;

import java.util.List;
import java.util.stream.Collectors;

public class FindScheduleDayImplService implements AbstractFindScheduleDayService {

    private final ScheduleDayRepository scheduleDayRepository;

    public FindScheduleDayImplService(ScheduleDayRepository scheduleDayRepository) {
        this.scheduleDayRepository = scheduleDayRepository;
    }

    @Override
    public List<ScheduleDayResponse> getScheduleDays(Long scheduleId) {
        List<ScheduleDayEntity> scheduleDayEntities = scheduleDayRepository.findAllByScheduleTravelEntityCodOrderByDay(scheduleId);
        return scheduleDayEntities.stream().map(this::scheduleDayToDTO).collect(Collectors.toList());
    }

    private ScheduleDayResponse scheduleDayToDTO(ScheduleDayEntity scheduleDay) {
        return new ScheduleDayResponse(scheduleDay);
    }
}
