package br.com.nicetravel.service.scheduleday.persist;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResponse;
import br.com.nicetravel.model.ScheduleDayEntity;

import javax.transaction.Transactional;

public abstract class AbstractScheduleDayService {

    @Transactional
    public abstract boolean deleteById(Long scheduleDayId);

    @Transactional
    public ScheduleDayResponse addScheduleDay(Long scheduleId){
        ScheduleDayEntity scheduleDayEntity = saveScheduleDay(scheduleId);
        return createScheduleDayDTO(scheduleDayEntity);
    }

    public abstract ScheduleDayEntity saveScheduleDay(Long scheduleId);

    protected abstract ScheduleDayResponse createScheduleDayDTO(ScheduleDayEntity scheduleDayEntity);

    public abstract void reorder(Long scheduleDayIdFrom, Long scheduleDayIdTo);
}
