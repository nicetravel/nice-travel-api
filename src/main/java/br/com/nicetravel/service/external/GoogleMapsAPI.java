package br.com.nicetravel.service.external;

import br.com.nicetravel.exceptions.GooglePlaceNotFoundException;
import br.com.nicetravel.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * API para o google MAPS/Place
 * <a href='https://developers.google.com/places/web-service/search#Fields'>Google Place API</a>
 */
@Service
public class GoogleMapsAPI {

    private final PropertiesUtil propertiesUtil;

    private static final String INVALID_REQUEST = "INVALID_REQUEST";
    private static final String URL_GOOGLE = "https://maps.googleapis.com/maps/api/place/details/json";
    private static final String URL_IMAGE_GOOGLE = "https://maps.googleapis.com/maps/api/place/photo";

    public GoogleMapsAPI(PropertiesUtil propertiesUtil) {
        this.propertiesUtil = propertiesUtil;
    }

    public PlaceDTO getPlaceDTO(String placeId) {
        String placeInformation;
        try {
            placeInformation = getPlaceInformation(placeId);
        } catch (IOException e) {
            throw new GooglePlaceNotFoundException("Não foi possível encontrar o local de ID: " + placeId);
        }

        JSONObject bodyObject = new JSONObject(Objects.requireNonNull(placeInformation));
        String status = bodyObject
                .optString("status");
        if (StringUtils.isEmpty(status) || INVALID_REQUEST.equals(status)) {
            throw new GooglePlaceNotFoundException(String.format("NOT FOUND A CITY WITH PLACE ID %s", placeId));
        }
        if (StringUtils.isBlank(bodyObject.optString("result"))) {
            throw new GooglePlaceNotFoundException(bodyObject.getString("error_message"));
        }
        JSONObject result = bodyObject.getJSONObject("result");

        List<String> imagesUrl = new ArrayList<>();
        if (StringUtils.isNotBlank(result.optString("photos"))) {
            result.getJSONArray("photos").forEach(ph -> imagesUrl.add(createImageURL((JSONObject) ph)));
        } else {
            imagesUrl.add("https://i.pinimg.com/736x/07/0e/67/070e67abfd0cb1c8fb9336ab5c44ec8a.jpg");
        }

        String formattedAddress = result.optString("formatted_address");
        JSONObject locationJson = result.getJSONObject("geometry").getJSONObject("location");
        String name = (String) result.get("name");

        Double lng = (Double) locationJson.get("lng");
        Double lat = (Double) locationJson.get("lat");

        String types = result.getJSONArray("types").join(",").replaceAll("\"", "");
        return new PlaceDTO()
                .setLng(lng)
                .setLat(lat)
                .setName(name)
                .setImageUrl(imagesUrl)
                .setTypes(types)
                .setFormattedAddress(formattedAddress);

    }

    private String createImageURL(JSONObject photos) {
        String imagePhotoReference = (String) photos.get("photo_reference");
        Integer imageWidth = (Integer) photos.get("width");

        UriBuilder builderImageUrl = UriBuilder.fromPath(URL_IMAGE_GOOGLE)
                .queryParam("maxwidth", imageWidth)
                .queryParam("photoreference", imagePhotoReference)
                .queryParam("key", propertiesUtil.getGoogleMapsKey());

        return builderImageUrl.toTemplate();
    }

    private String getPlaceInformation(String placeId) throws IOException {

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            UriBuilder builder = UriBuilder.fromUri(URL_GOOGLE)
                    .queryParam("language", "pt-BR")
                    .queryParam("placeid", placeId)
                    .queryParam("fields", "formatted_address,name,rating,geometry,photos,types")
                    .queryParam("key", propertiesUtil.getGoogleMapsKey());

            HttpGet request = new HttpGet(builder.toTemplate());
            request.addHeader("Accept", MediaType.APPLICATION_JSON_VALUE);

            try (CloseableHttpResponse response = httpClient.execute(request)) {

                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String result = EntityUtils.toString(entity);
                    if (response.getStatusLine().getStatusCode() == 200) {
                        return result;

                    } else {
                        throw new GooglePlaceNotFoundException("Não foi possível encontrar o local de ID: " + placeId + "\n" + result);
                    }
                }

            }
        }
        throw new GooglePlaceNotFoundException("Não foi possível encontrar o local de ID: " + placeId);
    }

}
