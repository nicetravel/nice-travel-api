package br.com.nicetravel.service.travel.retrieve;

import br.com.nicetravel.controller.schedule.ScheduleResponse;
import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.repository.ScheduleTravelRepository;
import br.com.nicetravel.repository.util.PagableUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.stream.Collectors;

public class FindTravelScheduleImplService implements AbstractFindTravelScheduleService {

    private final ScheduleTravelRepository scheduleTravelRepository;

    public FindTravelScheduleImplService(ScheduleTravelRepository scheduleTravelRepository) {
        this.scheduleTravelRepository = scheduleTravelRepository;
    }

    @Override
    public List<ScheduleResponse> getScheduleByPlaceID(@Nullable String placeID, Integer sizeElements) {
        List<ScheduleTravelEntity> scheduleTravelEntityList;
        if (StringUtils.isBlank(placeID)) {
            scheduleTravelEntityList = scheduleTravelRepository
                    .findByPublicAccessAndExclusionDateIsNull(Boolean.TRUE, PagableUtil.createPagable(0, sizeElements,
                            Sort.by(Sort.Order.desc("numberLike"), Sort.Order.asc("cityEntity.name"))));
        } else {
            scheduleTravelEntityList = scheduleTravelRepository
                    .findByCityEntityPlaceIDAndPublicAccessAndExclusionDateIsNull(placeID, Boolean.TRUE, PagableUtil.createPagable(0, sizeElements,
                            Sort.by(Sort.Order.desc("numberLike"), Sort.Order.asc("cityEntity.name"))));
        }
        return scheduleTravelEntityList.stream().map(this::scheduleEntityToDTO).collect(Collectors.toList());
    }

    @Override
    public List<ScheduleResponse> retrieveTravelScheduleByUserUID(String userUID) {
        return scheduleTravelRepository.findAllByUserOwnerUidAndExclusionDateIsNull(userUID)
                .stream().map(this::scheduleEntityToDTO).collect(Collectors.toList());
    }

    private ScheduleResponse scheduleEntityToDTO(ScheduleTravelEntity scheduleTravel) {
        return new ScheduleResponse(scheduleTravel);
    }

}
