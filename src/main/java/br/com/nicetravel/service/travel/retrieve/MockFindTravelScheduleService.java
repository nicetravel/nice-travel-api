package br.com.nicetravel.service.travel.retrieve;

import br.com.nicetravel.controller.schedule.ScheduleResponse;
import com.github.javafaker.Faker;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MockFindTravelScheduleService implements AbstractFindTravelScheduleService {

    @Override
    public List<ScheduleResponse> getScheduleByPlaceID(String placeID, Integer sizeElements) {
        List<ScheduleResponse> scheduleResponses = new ArrayList<>();
        for (int i = 1; i <= sizeElements; i++) {
            ScheduleResponse scheduleTravel = createScheduleTravel(placeID, new SecureRandom().nextInt(10) + 1);
            scheduleResponses.add(scheduleTravel);
        }
        return scheduleResponses;
    }

    @Override
    public List<ScheduleResponse> retrieveTravelScheduleByUserUID(String userUID) {
        List<ScheduleResponse> scheduleResponses = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            ScheduleResponse scheduleTravel = createScheduleTravel(new Faker().address().cityName(), new SecureRandom().nextInt(10) + 1);
            scheduleResponses.add(scheduleTravel);
        }
        return scheduleResponses;
    }

    private ScheduleResponse createScheduleTravel(String cityName, int qtdDias) {
        return new ScheduleResponse(qtdDias,
                Arrays.asList("https://s3.amazonaws.com/bk-static-prd-newctn/files/styles/discover_destaque/s3/2016-12/42%20-%20Salvador%20de%20Bahia_4.jpg?itok=2NW2cjVV"),
                cityName,
                qtdDias + 1L,
                BigDecimal.TEN,
                null,
                "Joaquim",
                0,
                true, "", "");
    }

}
