package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.controller.activity.ActivityResponse;
import br.com.nicetravel.controller.schedule.DuplicatedScheduleResponse;
import br.com.nicetravel.controller.schedule.ScheduleResponse;
import br.com.nicetravel.controller.scheduleday.ScheduleDayDuplicatedResponse;
import br.com.nicetravel.exceptions.IntegrationException;
import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.model.UserEntity;
import br.com.nicetravel.repository.ScheduleTravelRepository;
import br.com.nicetravel.repository.UserRepository;
import br.com.nicetravel.repository.VoteScheduleRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class TravelScheduleImplService extends AbstractTravelScheduleService {

    private final ScheduleTravelRepository scheduleTravelRepository;

    private final UserRepository userRepository;

    private final VoteScheduleRepository voteScheduleRepository;

    private final AbstractVoteTravelScheduleService voteTravelScheduleService;

    public TravelScheduleImplService(ScheduleTravelRepository scheduleTravelRepository,
                                     UserRepository userRepository,
                                     VoteScheduleRepository voteScheduleRepository,
                                     AbstractVoteTravelScheduleService voteTravelScheduleService) {
        this.scheduleTravelRepository = scheduleTravelRepository;
        this.userRepository = userRepository;
        this.voteScheduleRepository = voteScheduleRepository;
        this.voteTravelScheduleService = voteTravelScheduleService;
    }

    @Override
    @Transactional
    protected UserEntity saveOrUpdateUser(String userUID, String userEmail, String userName) {
        UserEntity userEntity = userRepository.findByUid(userUID).orElse(new UserEntity());
        userEntity.setUid(userUID);
        userEntity.setEmail(userEmail);
        userEntity.setName(userName);
        return userRepository.save(userEntity);
    }

    @Override
    protected DuplicatedScheduleResponse createScheduleDTO(ScheduleTravelEntity scheduleTravelEntity) {
        var days = scheduleTravelEntity.getScheduleDayEntities().stream()
                .map(s -> new ScheduleDayDuplicatedResponse(s.getDay(),
                        s.getCod(),
                        s.getActivities().stream().map(ActivityResponse::new).collect(Collectors.toList())))
                .collect(Collectors.toList());
        return new DuplicatedScheduleResponse(new ScheduleResponse(scheduleTravelEntity), days);
    }

    @Override
    @Transactional
    protected void voteTravelSchedule(Long scheduleId, UserEntity userEntity, Boolean positiveVote) {
        voteTravelScheduleService.voteTravelSchedule(scheduleId, userEntity, positiveVote);
    }

    @Override
    @Transactional
    public DuplicatedScheduleResponse duplicateSchedule(Long scheduleId, UserEntity userOwner) {
        ScheduleTravelEntity scheduleTravelBeDuplicated = scheduleTravelRepository.findById(scheduleId)
                .orElseThrow(() -> new IntegrationException("Não foi encontrado um cronograma com ID" + scheduleId));
        scheduleTravelBeDuplicated.getCityEntity().getPhotos(); /*Load Lazy*/
        return createScheduleDTO(scheduleTravelRepository.save(scheduleTravelBeDuplicated.duplicate(userOwner)));
    }

    @Override
    @Transactional
    public void logicalDelete(Long scheduleId) {
        ScheduleTravelEntity scheduleTravel = scheduleTravelRepository.findById(scheduleId)
                .orElseThrow(() -> new IntegrationException("Não foi encontrado um cronograma com ID" + scheduleId));
        scheduleTravel.setExclusionDate(LocalDate.now());
        scheduleTravelRepository.save(scheduleTravel);
    }

    @Override
    @Transactional
    public void delete(Long scheduleId) {
        voteScheduleRepository.deleteAllByScheduleTravelEntity_Cod(scheduleId);
        scheduleTravelRepository.deleteById(scheduleId);
    }

    @Override
    @Transactional
    public List<ScheduleTravelEntity> findAllRemovedSchedule(LocalDate dateExclusion) {
        return scheduleTravelRepository.findAllByExclusionDateBefore(dateExclusion);
    }

}
