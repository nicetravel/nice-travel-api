package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.controller.schedule.SchedulePublishResponse;
import br.com.nicetravel.controller.schedule.ScheduleResource;
import br.com.nicetravel.controller.scheduleday.ScheduleDayResource;
import br.com.nicetravel.model.CityEntity;
import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.model.UserEntity;
import org.springframework.lang.Nullable;

import java.util.List;

public abstract class AbstractPublishTravelScheduleService {

    public SchedulePublishResponse rePublishTravelSchedule(Long scheduleId, ScheduleResource schedule) {
        excludeLocalSchedule(scheduleId);
        SchedulePublishResponse scheduleRepublished = generateTravelSchedule(schedule);
        updateVotes(scheduleId, scheduleRepublished.getScheduleCod());
        return scheduleRepublished;
    }

    protected abstract void updateVotes(Long scheduleId, Long newScheduleRepublished);

    public SchedulePublishResponse generateTravelSchedule(ScheduleResource schedule) {
        CityEntity cityEntity = saveCity(schedule.placeID);
        UserEntity userOwner = saveOrUpdateUser(schedule.userUID, schedule.userEmail, schedule.userName);
        ScheduleTravelEntity scheduleTravelEntity = savePublishedScheduleTravel(cityEntity, schedule.scheduleDays, userOwner);
        return createPublish(scheduleTravelEntity);
    }

    protected abstract UserEntity saveOrUpdateUser(String userUID, @Nullable String userEmail, @Nullable String userName);

    protected abstract void excludeLocalSchedule(Long scheduleId);

    /**
     * This method have to save the  {@link ScheduleTravelEntity}.
     *
     * @param cityEntity
     * @param numberDays
     * @param userOwner
     * @return ScheduleTravelEntity
     */
    protected abstract ScheduleTravelEntity savePublishedScheduleTravel(CityEntity cityEntity, List<ScheduleDayResource> numberDays, UserEntity userOwner);

    /**
     * This method have to find city in Google API {@link br.com.nicetravel.service.external.GoogleMapsAPI} and save the City.
     * <p>
     * Note: This method have to verify if the city not exits
     *
     * @param placeID {@link br.com.nicetravel.service.external.GoogleMapsAPI}
     * @return CityEntity
     */
    protected abstract CityEntity saveCity(String placeID);

    protected abstract SchedulePublishResponse createPublish(ScheduleTravelEntity scheduleTravelEntity);

}
