package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.controller.schedule.DuplicatedScheduleResponse;
import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.model.UserEntity;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.util.List;


public abstract class AbstractTravelScheduleService {

    protected abstract UserEntity saveOrUpdateUser(String userUID, @Nullable String userEmail, @Nullable String userName);

    protected abstract DuplicatedScheduleResponse createScheduleDTO(ScheduleTravelEntity scheduleTravelEntity);

    protected abstract void voteTravelSchedule(Long scheduleId, UserEntity userUID, Boolean positiveVote);

    public abstract void delete(Long scheduleId);

    public abstract List<ScheduleTravelEntity> findAllRemovedSchedule(LocalDate dateExclusion);

    public abstract void logicalDelete(Long scheduleId);

    protected abstract DuplicatedScheduleResponse duplicateSchedule(Long scheduleId, UserEntity userOwner);

    public DuplicatedScheduleResponse duplicateSchedule(Long scheduleId, String userUID, String userEmail, String userName) {
        var user = saveOrUpdateUser(userUID, userEmail, userName);
        voteTravelSchedule(scheduleId, user, true);
        return duplicateSchedule(scheduleId, user);
    }

}
