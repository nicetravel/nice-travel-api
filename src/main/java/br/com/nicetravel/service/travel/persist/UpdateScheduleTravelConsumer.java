package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.model.ScheduleTravelEntity;

import java.util.function.Consumer;

public interface UpdateScheduleTravelConsumer extends Consumer<ScheduleTravelEntity> {
}
