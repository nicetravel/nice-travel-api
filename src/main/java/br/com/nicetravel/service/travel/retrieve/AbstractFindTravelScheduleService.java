package br.com.nicetravel.service.travel.retrieve;

import br.com.nicetravel.controller.schedule.ScheduleResponse;
import org.springframework.lang.Nullable;

import java.util.List;

public interface AbstractFindTravelScheduleService {

   List<ScheduleResponse> getScheduleByPlaceID(@Nullable String placeID, Integer sizeElements);

   List<ScheduleResponse> retrieveTravelScheduleByUserUID(String userUID);
}
