package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.controller.schedule.ActivityPublishResponse;
import br.com.nicetravel.controller.schedule.ScheduleDayPublishResponse;
import br.com.nicetravel.controller.schedule.SchedulePublishResponse;
import br.com.nicetravel.controller.scheduleday.ScheduleDayResource;
import br.com.nicetravel.model.*;
import br.com.nicetravel.model.enuns.StyleActivity;
import br.com.nicetravel.repository.CityRepository;
import br.com.nicetravel.repository.ScheduleTravelRepository;
import br.com.nicetravel.repository.TypeCityRepository;
import br.com.nicetravel.repository.UserRepository;
import br.com.nicetravel.service.external.GoogleMapsAPI;
import br.com.nicetravel.service.external.PlaceDTO;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PublishTravelScheduleImplService extends AbstractPublishTravelScheduleService {

    private final CityRepository cityRepository;

    private final TypeCityRepository typeCityRepository;

    private final GoogleMapsAPI googleMapsAPI;

    private final ScheduleTravelRepository scheduleTravelRepository;

    private final UserRepository userRepository;

    private final AbstractVoteTravelScheduleService voteTravelScheduleService;

    public PublishTravelScheduleImplService(CityRepository cityRepository,
                                            TypeCityRepository typeCityRepository,
                                            GoogleMapsAPI googleMapsAPI,
                                            ScheduleTravelRepository scheduleTravelRepository,
                                            UserRepository userRepository,
                                            AbstractVoteTravelScheduleService voteTravelScheduleService) {
        this.cityRepository = cityRepository;
        this.typeCityRepository = typeCityRepository;
        this.googleMapsAPI = googleMapsAPI;
        this.scheduleTravelRepository = scheduleTravelRepository;
        this.userRepository = userRepository;
        this.voteTravelScheduleService = voteTravelScheduleService;
    }

    @Override
    @Transactional
    protected UserEntity saveOrUpdateUser(String userUID, String userEmail, String userName) {
        UserEntity userEntity = userRepository.findByUid(userUID).orElse(new UserEntity());
        userEntity.setUid(userUID);
        userEntity.setEmail(userEmail);
        userEntity.setName(userName);
        return userRepository.save(userEntity);
    }

    @Override
    @Transactional
    protected void excludeLocalSchedule(Long scheduleId) {
        Optional<ScheduleTravelEntity> scheduleTravelOptional = scheduleTravelRepository.findById(scheduleId);
        if (scheduleTravelOptional.isPresent()) {
            ScheduleTravelEntity scheduleTravel = scheduleTravelOptional.get();
            scheduleTravel.setExclusionDate(LocalDate.now());
            scheduleTravelRepository.save(scheduleTravel);
        }
    }

    @Override
    @Transactional
    protected ScheduleTravelEntity savePublishedScheduleTravel(CityEntity cityEntity, List<ScheduleDayResource> scheduleResource, UserEntity userOwner) {
        ScheduleTravelEntity scheduleTravelEntity = new ScheduleTravelEntity();
        scheduleTravelEntity.setCityEntity(cityEntity);
        scheduleTravelEntity.setUserOwner(userOwner);
        scheduleTravelEntity.setPublicAccess(Boolean.TRUE);
        scheduleTravelEntity.setScheduleDayEntities(createScheduleDays(scheduleResource, scheduleTravelEntity));


        return scheduleTravelRepository.save(scheduleTravelEntity);
    }

    private List<ScheduleDayEntity> createScheduleDays(List<ScheduleDayResource> scheduleResource,
                                                       ScheduleTravelEntity scheduleTravelEntity) {
        return scheduleResource.stream().map(d -> {
            ScheduleDayEntity scheduleDayEntity = new ScheduleDayEntity();
            scheduleDayEntity.setDay(Integer.valueOf(d.day));
            scheduleDayEntity.setScheduleTravelEntity(scheduleTravelEntity);
            scheduleDayEntity.setActivities(createActivities(d, scheduleDayEntity));
            return scheduleDayEntity;
        }).collect(Collectors.toList());
    }

    private List<ActivityEntity> createActivities(ScheduleDayResource d, ScheduleDayEntity scheduleDayEntity) {
        return d.activities.stream().map(activity -> {
            ActivityEntity activityEntity = new ActivityEntity();
            activityEntity.setStyleActivity(StyleActivity.valueOfEnum(activity.styleActivity));
            activityEntity.setDtStart(activity.startActivity);
            activityEntity.setDtEnd(activity.finishActivity);
            activityEntity.setPrice(activity.price);
            activityEntity.setName(activity.nameOfPlace);
            activityEntity.setDescription(activity.description);
            activityEntity.setScheduleDayEntity(scheduleDayEntity);
            return activityEntity;
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    protected CityEntity saveCity(String placeID) {
        Optional<CityEntity> cityEntityOptional = cityRepository.findByPlaceID(placeID);
        if (cityEntityOptional.isPresent()) {
            return cityEntityOptional.get();
        }
        PlaceDTO placeDTO = googleMapsAPI.getPlaceDTO(placeID);
        CityEntity cityEntity = new CityEntity();
        cityEntity.setPlaceID(placeID);
        cityEntity.setPhotosLinks(placeDTO.getImageUrl());
        cityEntity.setName(placeDTO.getName());
        cityEntity.setLatitude(placeDTO.getLat());
        cityEntity.setLongitude(placeDTO.getLng());
        cityEntity.setFormattedAddress(placeDTO.getFormattedAddress());

        updateCityTypes(placeDTO, cityEntity);
        cityRepository.save(cityEntity);
        return cityEntity;
    }

    @Transactional
    void updateCityTypes(PlaceDTO placeDTO, CityEntity cityEntity) {
        if (placeDTO.getTypes() != null) {
            Set<TypeCityEntity> typeCityEntityList = new HashSet<>();
            Stream.of(placeDTO.getTypes().split(","))
                    .forEach(type -> typeCityRepository.findByDescription(type.trim()).ifPresent(typeCityEntityList::add));
            cityEntity.setTypeCities(typeCityEntityList);
        }
    }

    @Override
    protected SchedulePublishResponse createPublish(ScheduleTravelEntity scheduleTravel) {
        var days = scheduleTravel.getScheduleDayEntities()
                .stream()
                .map(scheduleDay -> {
                    var activities = scheduleDay.getActivities()
                            .stream()
                            .map(a -> new ActivityPublishResponse(a.getCod(), a.getDtStart()))
                            .collect(Collectors.toList());
                    return new ScheduleDayPublishResponse(scheduleDay.getCod(), scheduleDay.getDay(), activities);
                })
                .collect(Collectors.toList());
        return new SchedulePublishResponse(scheduleTravel.getCod(), scheduleTravel.getCityImageUrl(5), days);
    }


    @Override
    protected void updateVotes(Long scheduleId, Long newScheduleRepublished) {
        voteTravelScheduleService.transferVotes(scheduleId, newScheduleRepublished);
    }
}
