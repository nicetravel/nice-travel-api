package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.controller.schedule.RaitingResponse;
import br.com.nicetravel.model.ScheduleTravelEntity;
import br.com.nicetravel.model.UserEntity;
import br.com.nicetravel.model.VoteScheduleEntity;
import br.com.nicetravel.repository.ScheduleTravelRepository;
import br.com.nicetravel.repository.UserRepository;
import br.com.nicetravel.repository.VoteScheduleRepository;
import br.com.nicetravel.repository.util.PagableUtil;
import org.springframework.data.domain.Sort;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VoteTravelScheduleImplService extends AbstractVoteTravelScheduleService {

    private final ScheduleTravelRepository scheduleTravelRepository;

    private final UserRepository userRepository;

    private final VoteScheduleRepository voteScheduleRepository;

    public VoteTravelScheduleImplService(ScheduleTravelRepository scheduleTravelRepository,
                                         UserRepository userRepository,
                                         VoteScheduleRepository voteScheduleRepository) {
        this.scheduleTravelRepository = scheduleTravelRepository;
        this.userRepository = userRepository;
        this.voteScheduleRepository = voteScheduleRepository;
    }

    @Override
    @Transactional
    protected UserEntity findOrCreateUser(String userUID, String userEmail, String userName) {
        Optional<UserEntity> user = userRepository.findByUid(userUID);
        if (user.isPresent()) {
            return user.get();
        } else {
            UserEntity userEntity = new UserEntity();
            userEntity.setUid(userUID);
            userEntity.setEmail(userEmail);
            userEntity.setName(userName);
            return userRepository.save(userEntity);
        }
    }

    @Override
    @Transactional
    public boolean voteTravelSchedule(Long scheduleId, UserEntity userEntity, Boolean positiveVote) {
        if (voteScheduleRepository.countAllByUserVoteAndScheduleTravelEntity_Cod(userEntity, scheduleId) > 0) {
            return false;
        }
        return updateScheduleTravel(scheduleId, scheduleTravel -> {
            int vote = positiveVote ? 1 : -1;
            scheduleTravel.setNumberLike(scheduleTravel.getNumberLike() + vote);
            createVoteSchedule(userEntity, scheduleTravel);
        });
    }

    @Override
    @Transactional
    public boolean commentaryTravelSchedule(Long scheduleId, UserEntity userEntity, String commentary, Double qtdStar) {
        boolean isNewVote;
        Optional<VoteScheduleEntity> voteOpt = voteScheduleRepository.findByUserVoteAndScheduleTravelEntity_Cod(userEntity, scheduleId);
        VoteScheduleEntity voteScheduleEntity;
        if (voteOpt.isPresent()) {
            voteScheduleEntity = voteOpt.get();
            isNewVote = false;
        } else {
            voteScheduleEntity = new VoteScheduleEntity();
            Optional<ScheduleTravelEntity> scheduleTravelOptional = scheduleTravelRepository.findById(scheduleId);
            scheduleTravelOptional.ifPresent(voteScheduleEntity::setScheduleTravelEntity);
            voteScheduleEntity.setLike(false);
            voteScheduleEntity.setUserVote(userEntity);
            isNewVote = true;
        }
        voteScheduleEntity.setDtVote(LocalDate.now());
        voteScheduleEntity.setCommentary(commentary);
        voteScheduleEntity.setQtdStart(qtdStar);
        voteScheduleRepository.save(voteScheduleEntity);

        if (isNewVote && qtdStar >= 3) {
            updateScheduleTravel(scheduleId, scheduleTravel -> scheduleTravel.setNumberLike(scheduleTravel.getNumberLike() + 1));
        }
        return isNewVote;
    }

    @Override
    public void transferVotes(Long scheduleId, Long newScheduleRepublished) {
        int votes = scheduleTravelRepository.findById(scheduleId).map(ScheduleTravelEntity::getNumberLike)
                .orElse(0);

        scheduleTravelRepository.findById(newScheduleRepublished).ifPresent(schedule -> {
            schedule.setNumberLike(Math.toIntExact(votes));
            scheduleTravelRepository.save(schedule);
        });
        voteScheduleRepository.changeVotes(scheduleId, newScheduleRepublished);
    }

    @Override
    public List<RaitingResponse> findCommentaries(Long scheduleId, Integer sizeElements) {
        return voteScheduleRepository
                .findAllByScheduleTravelEntityCod(scheduleId, PagableUtil.createPagable(0, sizeElements,
                        Sort.by(Sort.Order.desc("dtVote"))))
                .stream()
                .filter(s -> s.getQtdStart() != null)
                .map(RaitingResponse::new)
                .collect(Collectors.toList());
    }

    private void createVoteSchedule(UserEntity userEntity, ScheduleTravelEntity scheduleTravel) {
        VoteScheduleEntity voteScheduleEntity = new VoteScheduleEntity();
        voteScheduleEntity.setScheduleTravelEntity(scheduleTravel);
        voteScheduleEntity.setUserVote(userEntity);
        voteScheduleEntity.setLike(true);
        voteScheduleRepository.save(voteScheduleEntity);
    }

    private boolean updateScheduleTravel(Long scheduleId, UpdateScheduleTravelConsumer consumer) {
        Optional<ScheduleTravelEntity> scheduleTravelOptional = scheduleTravelRepository.findById(scheduleId);
        if (scheduleTravelOptional.isPresent()) {
            ScheduleTravelEntity scheduleTravel = scheduleTravelOptional.get();
            consumer.accept(scheduleTravel);
            scheduleTravelRepository.save(scheduleTravel);
            return true;
        }
        return false;
    }

}
