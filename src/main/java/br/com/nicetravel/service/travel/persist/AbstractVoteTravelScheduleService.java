package br.com.nicetravel.service.travel.persist;

import br.com.nicetravel.controller.schedule.RaitingResponse;
import br.com.nicetravel.model.UserEntity;

import java.util.List;


public abstract class AbstractVoteTravelScheduleService {

    protected abstract UserEntity findOrCreateUser(String userUID, String userEmail, String userName);

    public abstract boolean voteTravelSchedule(Long scheduleId, UserEntity userUID, Boolean positiveVote);

    public abstract boolean commentaryTravelSchedule(Long scheduleId, UserEntity userUID, String commentary, Double qtdStar);

    public boolean commentaryTravelSchedule(Long scheduleId,String userUID, String email, String displayName, String commentary, Double qtdStar) {
        UserEntity user = findOrCreateUser(userUID, email, displayName);
        return commentaryTravelSchedule(scheduleId, user, commentary, qtdStar);
    }

    public boolean voteTravelSchedule(Long scheduleId, String userUID, String email, String displayName, Boolean positiveVote) {
        UserEntity user = findOrCreateUser(userUID, email, displayName);
        return voteTravelSchedule(scheduleId, user, positiveVote);
    }

    public abstract void transferVotes(Long scheduleId, Long newScheduleRepublished);

    public abstract List<RaitingResponse> findCommentaries(Long scheduleId, Integer sizeElements);
}
