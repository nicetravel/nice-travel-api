package br.com.nicetravel.service.activity.persist;

import br.com.nicetravel.controller.activity.ActivityResponse;
import br.com.nicetravel.model.ActivityEntity;
import br.com.nicetravel.model.enuns.StyleActivity;
import br.com.nicetravel.repository.ActivityRepository;
import br.com.nicetravel.repository.ScheduleDayRepository;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;
import java.time.LocalTime;


public class ActivityImplService implements AbstractActivityService {

    private final ActivityRepository activityRepository;

    private final ScheduleDayRepository scheduleDayRepository;

    public ActivityImplService(ActivityRepository activityRepository, ScheduleDayRepository scheduleDayRepository) {
        this.activityRepository = activityRepository;
        this.scheduleDayRepository = scheduleDayRepository;
    }

    @Override
    public ActivityResponse saveActivity(String description, String nameOfPlace, BigDecimal price, LocalTime startActivity,
                                         LocalTime finishActivity, String styleActivity, Long idScheduleDay, @Nullable Long id) {

        ActivityEntity activityEntity;
        if (id != null) {
            activityEntity = activityRepository.getOne(id);
        } else {
            activityEntity = new ActivityEntity();
        }
        activityEntity.setName(nameOfPlace);
        activityEntity.setStyleActivity(StyleActivity.valueOfEnum(styleActivity));
        activityEntity.setPrice(price);
        activityEntity.setDtStart(startActivity);
        activityEntity.setDtEnd(finishActivity);
        activityEntity.setDescription(description);
        activityEntity.setScheduleDayEntity(scheduleDayRepository.getOne(idScheduleDay));
        ActivityEntity activitySaved = activityRepository.save(activityEntity);
        return new ActivityResponse(activitySaved);
    }

    @Override
    public boolean delete(Long activityId) {
        boolean exists =activityRepository.existsById(activityId);
        activityRepository.deleteById(activityId);
        return exists;
    }
}
