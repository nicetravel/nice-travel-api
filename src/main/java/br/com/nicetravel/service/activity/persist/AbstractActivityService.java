package br.com.nicetravel.service.activity.persist;

import br.com.nicetravel.controller.activity.ActivityResponse;
import org.springframework.lang.Nullable;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalTime;

public interface AbstractActivityService {

    @Transactional
    ActivityResponse saveActivity(String description,
                                  String nameOfPlace,
                                  BigDecimal price,
                                  LocalTime startActivity,
                                  LocalTime finishActivity,
                                  String styleActivity,
                                  Long idScheduleDay,
                                  @Nullable Long id);

    @Transactional
    boolean delete(Long activityId);
}
