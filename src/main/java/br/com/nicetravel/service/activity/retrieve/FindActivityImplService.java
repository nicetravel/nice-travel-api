package br.com.nicetravel.service.activity.retrieve;

import br.com.nicetravel.controller.activity.ActivityResponse;
import br.com.nicetravel.model.ActivityEntity;
import br.com.nicetravel.repository.ActivityRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class FindActivityImplService implements AbstractFindActivityService {

    private final ActivityRepository activityRepository;

    public FindActivityImplService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public List<ActivityResponse> getActivities(Long scheduleDayId) {
        List<ActivityEntity> activities = activityRepository.findAllByScheduleDayEntityCod(scheduleDayId);
        return activities
                .stream()
                .map(this::activityEntityToDTO)
                .sorted(Comparator.comparing(ActivityResponse::getStartActivity))
                .collect(Collectors.toList());
    }


    private ActivityResponse activityEntityToDTO(ActivityEntity activityEntity) {
        return new ActivityResponse(activityEntity);
    }
}
