package br.com.nicetravel.service.activity.retrieve;

import br.com.nicetravel.controller.activity.ActivityResponse;
import br.com.nicetravel.model.enuns.StyleActivity;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MockFindActivityService implements AbstractFindActivityService {

    @Override
    public List<ActivityResponse> getActivities(Long scheduleDayId) {
        return createActivities();
    }

    private List<ActivityResponse> createActivities() {
        List<ActivityResponse> activityResponseList = new ArrayList<>();

        activityResponseList.add(new ActivityResponse("Faby Buggy apresenta o que tem de melhor no nosso lindo litoral Sul ,PORTO DE GALINHAS/PE e Regiões , " +
                "aqui vocês vão encontrar as opções de Lazer,cultura e Diversões .. Somos uma equipe que trabalhamos para seu " +
                "lazer e sossego com total segurança Responsabilidade e pontualidade",
                "Faby Buggy Turismo",
                        BigDecimal.valueOf(new SecureRandom().nextInt(250) + 100.21),
                LocalTime.of(7, 30),
                LocalTime.of(18, 00),
                StyleActivity.OTHER.getDescription(),
                1L,
                1L)
        );

        activityResponseList.add(new ActivityResponse("Tomar Banho e se arrumar",
                "Tomar um banho e se prepara para noite.",
                BigDecimal.ZERO,
                LocalTime.of(18, 0),
                LocalTime.of(19, 30),
                StyleActivity.OTHER.getDescription(),
                2L,
                2L)
        );
        return activityResponseList;
    }
}
