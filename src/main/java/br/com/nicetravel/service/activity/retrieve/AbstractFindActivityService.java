package br.com.nicetravel.service.activity.retrieve;

import br.com.nicetravel.controller.activity.ActivityResponse;

import java.util.List;

public interface AbstractFindActivityService {

    List<ActivityResponse> getActivities(Long scheduleDayId);

}
