package br.com.nicetravel.service.activity.persist;

import br.com.nicetravel.controller.activity.ActivityResponse;

import java.math.BigDecimal;
import java.time.LocalTime;

public class MockActivityService implements AbstractActivityService {

    @Override
    public ActivityResponse saveActivity(String description, String nameOfPlace, BigDecimal price, LocalTime startActivity,
                                         LocalTime finishActivity, String styleActivity, Long idScheduleDay, Long id) {
        return new ActivityResponse(description,
                nameOfPlace, price,
                startActivity, finishActivity,
                styleActivity, idScheduleDay, id);
    }

    @Override
    public boolean delete(Long activityId) {
        return false;
        //NOOP
    }
}
