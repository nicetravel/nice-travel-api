package br.com.nicetravel.util;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("nice-travel")
public interface PropertiesUtil {

    @ConfigProperty(name = "develop.enabled")
    boolean isDevelopMode();

    @ConfigProperty(name = "google.api-key")
    String getGoogleMapsKey();

}
