package br.com.nicetravel.repository;

import br.com.nicetravel.model.ScheduleDayEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScheduleDayRepository extends JpaRepository<ScheduleDayEntity, Long> {

    List<ScheduleDayEntity> findAllByScheduleTravelEntityCodOrderByDay(Long scheduleTravelCod);
}
