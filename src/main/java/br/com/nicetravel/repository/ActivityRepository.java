package br.com.nicetravel.repository;

import br.com.nicetravel.model.ActivityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActivityRepository extends JpaRepository<ActivityEntity, Long> {

    List<ActivityEntity> findAllByScheduleDayEntityCod(Long scheduleDayId);
}
