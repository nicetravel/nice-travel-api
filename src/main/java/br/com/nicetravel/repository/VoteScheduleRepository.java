package br.com.nicetravel.repository;

import br.com.nicetravel.model.UserEntity;
import br.com.nicetravel.model.VoteScheduleEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface VoteScheduleRepository extends JpaRepository<VoteScheduleEntity, Long> {

    List<VoteScheduleEntity> findAllByScheduleTravelEntityCod(Long scheduleCod, Pageable pageable);

    long countAllByUserVoteAndScheduleTravelEntity_Cod(UserEntity user, Long scheduleCod);

    Optional<VoteScheduleEntity> findByUserVoteAndScheduleTravelEntity_Cod(UserEntity user, Long scheduleTravel);

    @Modifying
    long deleteAllByScheduleTravelEntity_Cod(Long scheduleCod);

    @Modifying
    @Query("UPDATE VoteScheduleEntity SET scheduleTravelEntity.cod = :newScheduleRepublished " +
            "WHERE scheduleTravelEntity.cod = :scheduleId")
    void changeVotes(@Param("scheduleId") Long scheduleId, @Param("newScheduleRepublished") Long newScheduleRepublished);
}
