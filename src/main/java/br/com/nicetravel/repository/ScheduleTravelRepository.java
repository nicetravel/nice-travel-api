package br.com.nicetravel.repository;

import br.com.nicetravel.model.ScheduleTravelEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleTravelRepository extends JpaRepository<ScheduleTravelEntity, Long> {

    List<ScheduleTravelEntity> findByCityEntityPlaceIDAndPublicAccessAndExclusionDateIsNull(String placeID, Boolean publicAccess, Pageable pageable);
    List<ScheduleTravelEntity> findByPublicAccessAndExclusionDateIsNull(Boolean publicAccess, Pageable pageable);

    List<ScheduleTravelEntity> findAllByUserOwnerUidAndExclusionDateIsNull(String userUID);


    List<ScheduleTravelEntity> findAllByExclusionDateBefore(LocalDate dateExclusion);
}
