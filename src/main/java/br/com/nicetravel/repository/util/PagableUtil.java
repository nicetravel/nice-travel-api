package br.com.nicetravel.repository.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PagableUtil {

    public static Pageable createPagable(int page, int size) {
        return PageRequest.of(page, size);
    }

    public static Pageable createPagable(int page, int size, Sort sort) {
        return PageRequest.of(page, size, sort);
    }
}
