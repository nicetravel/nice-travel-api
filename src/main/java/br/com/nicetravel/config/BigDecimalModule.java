package br.com.nicetravel.config;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.math.BigDecimal;

public class BigDecimalModule extends SimpleModule {

    BigDecimalModule(){
        addSerializer(BigDecimal.class, new ToStringSerializer());
    }
}
