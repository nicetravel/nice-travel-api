package br.com.nicetravel.config;

import br.com.nicetravel.repository.*;
import br.com.nicetravel.service.activity.persist.AbstractActivityService;
import br.com.nicetravel.service.activity.persist.ActivityImplService;
import br.com.nicetravel.service.activity.persist.MockActivityService;
import br.com.nicetravel.service.activity.retrieve.AbstractFindActivityService;
import br.com.nicetravel.service.activity.retrieve.FindActivityImplService;
import br.com.nicetravel.service.activity.retrieve.MockFindActivityService;
import br.com.nicetravel.service.external.GoogleMapsAPI;
import br.com.nicetravel.service.scheduleday.persist.AbstractScheduleDayService;
import br.com.nicetravel.service.scheduleday.persist.MockScheduleDayService;
import br.com.nicetravel.service.scheduleday.persist.ScheduleDayImplService;
import br.com.nicetravel.service.scheduleday.retrieve.AbstractFindScheduleDayService;
import br.com.nicetravel.service.scheduleday.retrieve.FindScheduleDayImplService;
import br.com.nicetravel.service.scheduleday.retrieve.MockFindScheduleDayService;
import br.com.nicetravel.service.travel.persist.*;
import br.com.nicetravel.service.travel.retrieve.AbstractFindTravelScheduleService;
import br.com.nicetravel.service.travel.retrieve.FindTravelScheduleImplService;
import br.com.nicetravel.service.travel.retrieve.MockFindTravelScheduleService;
import br.com.nicetravel.util.PropertiesUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanFactory {

    private final PropertiesUtil propertiesUtil;

    public BeanFactory(PropertiesUtil propertiesUtil) {
        this.propertiesUtil = propertiesUtil;
    }

    @Bean
    public AbstractTravelScheduleService travelSchedule(ScheduleTravelRepository scheduleTravelRepository,
                                                        UserRepository userRepository,
                                                        VoteScheduleRepository voteScheduleRepository,
                                                        AbstractVoteTravelScheduleService voteTravelScheduleService) {
        return new TravelScheduleImplService(scheduleTravelRepository, userRepository, voteScheduleRepository, voteTravelScheduleService);
    }

    @Bean
    public AbstractPublishTravelScheduleService publishTravelSchedule(CityRepository cityRepository,
                                                                      TypeCityRepository typeCityRepository,
                                                                      GoogleMapsAPI googleMapsAPI,
                                                                      ScheduleTravelRepository scheduleTravelRepository,
                                                                      UserRepository userRepository,
                                                                      AbstractVoteTravelScheduleService voteTravelScheduleService) {
        return new PublishTravelScheduleImplService(cityRepository, typeCityRepository, googleMapsAPI,
                scheduleTravelRepository, userRepository, voteTravelScheduleService);
    }

    @Bean
    public AbstractVoteTravelScheduleService voteTravelScheduleService(ScheduleTravelRepository scheduleTravelRepository,
                                                                       UserRepository userRepository,
                                                                       VoteScheduleRepository voteScheduleRepository) {
        return new VoteTravelScheduleImplService(scheduleTravelRepository, userRepository, voteScheduleRepository);
    }

    @Bean
    public AbstractFindTravelScheduleService findTravelSchedule(ScheduleTravelRepository scheduleTravelRepository) {
        if (propertiesUtil.isDevelopMode()) {
            return new MockFindTravelScheduleService();
        }
        return new FindTravelScheduleImplService(scheduleTravelRepository);
    }

    @Bean
    public AbstractScheduleDayService scheduleDay(ScheduleDayRepository scheduleDayRepository,
                                                  ScheduleTravelRepository scheduleRepository) {
        if (propertiesUtil.isDevelopMode()) {
            return new MockScheduleDayService();
        }
        return new ScheduleDayImplService(scheduleDayRepository, scheduleRepository);
    }

    @Bean
    public AbstractFindScheduleDayService findScheduleDay(ScheduleDayRepository scheduleDayRepository) {
        if (propertiesUtil.isDevelopMode()) {
            return new MockFindScheduleDayService();
        }
        return new FindScheduleDayImplService(scheduleDayRepository);
    }

    @Bean
    public AbstractFindActivityService findActivityService(ActivityRepository activityRepository) {
        if (propertiesUtil.isDevelopMode()) {
            return new MockFindActivityService();
        }
        return new FindActivityImplService(activityRepository);
    }

    @Bean
    public AbstractActivityService activityService(ActivityRepository activityRepository,
                                                   ScheduleDayRepository scheduleDayRepository) {
        if (propertiesUtil.isDevelopMode()) {
            return new MockActivityService();
        }
        return new ActivityImplService(activityRepository, scheduleDayRepository);
    }

}
