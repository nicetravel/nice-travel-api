package br.com.nicetravel.config;

import javax.ws.rs.ext.ParamConverter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalDateConverter implements ParamConverter<LocalTime> {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    @Override
    public LocalTime fromString(String value) {
        return LocalTime.parse(value, DATE_TIME_FORMATTER);
    }

    @Override
    public String toString(LocalTime value) {
        return DATE_TIME_FORMATTER.format(value);
    }

}
