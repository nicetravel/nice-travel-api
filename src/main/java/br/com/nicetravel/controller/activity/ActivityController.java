package br.com.nicetravel.controller.activity;

import br.com.nicetravel.service.activity.persist.AbstractActivityService;
import br.com.nicetravel.service.activity.retrieve.AbstractFindActivityService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/activities")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Activity")
@SecurityScheme(securitySchemeName = "Basic Auth", type = SecuritySchemeType.HTTP, scheme = "basic")
public class ActivityController {

    private final AbstractFindActivityService abstractFindActivityService;

    private final AbstractActivityService abstractActivityService;

    public ActivityController(AbstractFindActivityService abstractFindActivityService,
                              AbstractActivityService abstractActivityService) {
        this.abstractFindActivityService = abstractFindActivityService;
        this.abstractActivityService = abstractActivityService;
    }

    @GET
    @Path("/{scheduleDayId}")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Return List of activities",
                    content = @Content(mediaType = "application/json")),})
    @Operation(summary = "Get the Activities according cod of schedule day")
    @RolesAllowed("user")
    public List<ActivityResponse> getActivities(@PathParam("scheduleDayId") @Valid @NotNull Long scheduleDayId) {
        return abstractFindActivityService.getActivities(scheduleDayId);
    }

    @POST
    @APIResponses(value = {
            @APIResponse(responseCode = "201", description = "Return activity add",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Add or Update activity")
    @RolesAllowed("user")
    public Response addActivity(@Valid ActivityResource activityResource) {
        ActivityResponse activityResponse = abstractActivityService.saveActivity(
                activityResource.description,
                activityResource.nameOfPlace,
                activityResource.price,
                activityResource.startActivity,
                activityResource.finishActivity,
                activityResource.styleActivity,
                activityResource.idScheduleDay,
                activityResource.id
        );
        return Response.accepted(activityResponse).status(201).build();
    }

    @DELETE
    @Path("/{activityId}")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Delete activity",
                    content = @Content(mediaType = "application/json")),
            @APIResponse(responseCode = "404", description = "Not found activity",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Delete activity")
    @RolesAllowed("user")
    public Response deleteActivity(@PathParam("activityId") @Valid @NotNull Long activityId) {
        boolean deleted = abstractActivityService.delete(activityId);
        if(deleted) {
            return Response.status(204).build();
        }
        return Response.status(404).build();
    }
}
