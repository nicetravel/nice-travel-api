package br.com.nicetravel.controller.activity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalTime;

public class ActivityResource {

    @NotBlank(message = "Campo Obrigatório")
    public String description;

    @NotBlank(message = "Campo Obrigatório")
    public String nameOfPlace;

    @NotNull(message = "Campo Obrigatório")
    public BigDecimal price;

    @NotNull(message = "Campo Obrigatório")
    public LocalTime startActivity;

    @NotNull(message = "Campo Obrigatório")
    public LocalTime finishActivity;

    @NotBlank(message = "Campo Obrigatório")
    public String styleActivity;

    @NotNull(message = "Campo Obrigatório")
    public Long idScheduleDay;

    public Long id;

}
