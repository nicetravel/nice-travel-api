package br.com.nicetravel.controller.activity;

import br.com.nicetravel.model.ActivityEntity;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Optional;

public class ActivityResponse {

    private final String description;
    private final String nameOfPlace;
    private final BigDecimal price;
    private final LocalTime startActivity;
    private final LocalTime finishActivity;
    private final String styleActivity;
    private final Long idScheduleDay;
    private final Long id;

    public ActivityResponse(ActivityEntity activityEntity) {
        this(activityEntity.getDescription(), activityEntity.getName(),
                Optional.ofNullable(activityEntity.getPrice()).orElse(BigDecimal.ZERO),
                activityEntity.getDtStart(), activityEntity.getDtEnd(),
                activityEntity.getStyleActivity().getDescription(),
                activityEntity.getScheduleDayEntity().getCod(),
                activityEntity.getCod());

    }

    public ActivityResponse(String description, String nameOfPlace, BigDecimal price,
                            LocalTime startActivity, LocalTime finishActivity,
                            String styleActivity, Long idScheduleDay, Long id) {
        this.description = description;
        this.nameOfPlace = nameOfPlace;
        this.price = price;
        this.startActivity = startActivity;
        this.finishActivity = finishActivity;
        this.styleActivity = styleActivity;
        this.idScheduleDay = idScheduleDay;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getNameOfPlace() {
        return nameOfPlace;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalTime getStartActivity() {
        return startActivity;
    }

    public LocalTime getFinishActivity() {
        return finishActivity;
    }

    public String getStyleActivity() {
        return styleActivity;
    }

    public Long getIdScheduleDay() {
        return idScheduleDay;
    }

    public Long getId() {
        return id;
    }
}
