package br.com.nicetravel.controller.scheduleday;

import br.com.nicetravel.controller.activity.ActivityResponse;

import java.util.List;

public class ScheduleDayDuplicatedResponse {
    public final int day;
    public final Long id;
    public final List<ActivityResponse> activities;

    public ScheduleDayDuplicatedResponse(int day,
                                         Long id,
                                         List<ActivityResponse> activities) {
        this.day = day;
        this.id = id;
        this.activities = activities;
    }

    public int getDay() {
        return day;
    }

    public Long getId() {
        return id;
    }

    public List<ActivityResponse> getActivities() {
        return activities;
    }
}
