package br.com.nicetravel.controller.scheduleday;

import br.com.nicetravel.controller.activity.ActivityResource;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class ScheduleDayResource {
    @NotBlank(message = "Campo Obrigatório")
    public String day;
    public int id;

    public List<ActivityResource> activities;

}
