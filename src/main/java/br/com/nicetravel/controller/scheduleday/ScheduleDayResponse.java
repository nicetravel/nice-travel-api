package br.com.nicetravel.controller.scheduleday;

import br.com.nicetravel.model.ScheduleDayEntity;

import java.math.BigDecimal;

public class ScheduleDayResponse {

    private final Long id;
    private final int day;
    private final BigDecimal priceDay;
    private final int qtdActivities;
    private final String typeFirstActivity;

    public ScheduleDayResponse(ScheduleDayEntity scheduleDay) {
        this.id = scheduleDay.getCod();
        this.day = scheduleDay.getDay();
        this.priceDay = scheduleDay.getPriceDay();
        this.qtdActivities = scheduleDay.getQtdActivities();
        this.typeFirstActivity = scheduleDay.getFirstActivity();
    }

    public int getDay() {
        return day;
    }

    public BigDecimal getPriceDay() {
        return priceDay;
    }

    public Long getId() {
        return id;
    }

    public int getQtdActivities() {
        return qtdActivities;
    }

    public String getTypeFirstActivity() {
        return typeFirstActivity;
    }
}
