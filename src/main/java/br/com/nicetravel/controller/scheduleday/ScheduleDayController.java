package br.com.nicetravel.controller.scheduleday;

import br.com.nicetravel.service.scheduleday.persist.AbstractScheduleDayService;
import br.com.nicetravel.service.scheduleday.retrieve.AbstractFindScheduleDayService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/schedulesDay")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Schedule Day")
@SecurityScheme(securitySchemeName = "Basic Auth", type = SecuritySchemeType.HTTP, scheme = "basic")
public class ScheduleDayController {

    private final AbstractScheduleDayService scheduleDayService;

    private final AbstractFindScheduleDayService findScheduleDayService;

    public ScheduleDayController(AbstractScheduleDayService scheduleDayService,
                                 AbstractFindScheduleDayService findScheduleDayService) {
        this.scheduleDayService = scheduleDayService;
        this.findScheduleDayService = findScheduleDayService;
    }

    @GET
    @Path("/{scheduleId}")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Return List schedule days",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Retrieve Schedules days by schedule id")
    @RolesAllowed("user")
    public List<ScheduleDayResponse> getScheduleDaysByScheduleCod(@PathParam("scheduleId") @Valid @NotNull Long scheduleId) {
        return findScheduleDayService.getScheduleDays(scheduleId);
    }

    @POST
    @Path("/{scheduleId}")
    @APIResponses(value = {
            @APIResponse(responseCode = "201", description = "Return new schedule day",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Add Schedule day")
    @RolesAllowed("user")
    public Response addScheduleDay(@PathParam("scheduleId") @Valid @NotNull Long scheduleId) {
        ScheduleDayResponse scheduleDayResponse = scheduleDayService.addScheduleDay(scheduleId);
        return Response.accepted(scheduleDayResponse).status(201).build();
    }

    @DELETE
    @Path("/{scheduleDayId}")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Delete schedule day",
                    content = @Content(mediaType = "application/json")),
            @APIResponse(responseCode = "404", description = "Not found schedule day",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Delete activity")
    @RolesAllowed("user")
    public Response deleteActivity(@PathParam("scheduleDayId") @Valid @NotNull Long scheduleDayId) {
        boolean deleted = scheduleDayService.deleteById(scheduleDayId);
        if (deleted) {
            return Response.status(204).build();
        }
        return Response.status(404).build();
    }

    @POST
    @Path("/reorder")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Reorder schedules days",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Reorder schedule days")
    @RolesAllowed("user")
    public Response reorderScheduleDays(@Valid ScheduleDayReorderResource scheduleDayReorder) {
        scheduleDayService.reorder(scheduleDayReorder.scheduleDayIdFrom, scheduleDayReorder.scheduleDayIdTo);
        return Response.status(204).build();
    }

}
