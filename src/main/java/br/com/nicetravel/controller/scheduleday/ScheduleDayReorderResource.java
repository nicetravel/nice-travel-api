package br.com.nicetravel.controller.scheduleday;

import javax.validation.constraints.NotNull;

public class ScheduleDayReorderResource {

    @NotNull(message = "Campo Obrigatório")
    public Long scheduleDayIdFrom;

    @NotNull(message = "Campo Obrigatório")
    public Long scheduleDayIdTo;

}
