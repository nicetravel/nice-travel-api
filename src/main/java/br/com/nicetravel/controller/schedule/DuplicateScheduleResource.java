package br.com.nicetravel.controller.schedule;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DuplicateScheduleResource {

    @NotNull(message = "Campo Obrigatório")
    public Long scheduleId;
    @NotBlank(message = "Campo Obrigatório")
    public String userUID;
    @NotBlank(message = "Campo Obrigatório")
    public String userEmail;
    @NotBlank(message = "Campo Obrigatório")
    public String userName;
}
