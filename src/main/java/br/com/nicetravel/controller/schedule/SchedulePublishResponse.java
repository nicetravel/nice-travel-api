package br.com.nicetravel.controller.schedule;

import java.util.List;

public class SchedulePublishResponse {

    private final Long scheduleCod;
    private final List<String> imagesUrl;

    public final List<ScheduleDayPublishResponse> scheduleDays;

    public SchedulePublishResponse(Long scheduleCod, List<String> imagesUrl, List<ScheduleDayPublishResponse> scheduleDays) {
        this.scheduleCod = scheduleCod;
        this.imagesUrl = imagesUrl;
        this.scheduleDays = scheduleDays;
    }

    public Long getScheduleCod() {
        return scheduleCod;
    }

    public List<ScheduleDayPublishResponse> getScheduleDays() {
        return scheduleDays;
    }

    public List<String> getImagesUrl() {
        return imagesUrl;
    }
}
