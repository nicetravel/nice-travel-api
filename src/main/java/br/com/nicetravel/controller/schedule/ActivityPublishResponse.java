package br.com.nicetravel.controller.schedule;

import java.time.LocalTime;

public class ActivityPublishResponse {

    private final Long id;
    private final LocalTime startActivity;

    public ActivityPublishResponse(Long id, LocalTime startActivity) {
        this.id = id;
        this.startActivity = startActivity;
    }

    public Long getId() {
        return id;
    }

    public LocalTime getStartActivity() {
        return startActivity;
    }
}
