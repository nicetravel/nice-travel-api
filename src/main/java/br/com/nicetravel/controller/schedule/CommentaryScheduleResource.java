package br.com.nicetravel.controller.schedule;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CommentaryScheduleResource {

    @NotNull(message = "Campo Obrigatório")
    public Long scheduleId;
    @NotBlank(message = "Campo Obrigatório")
    public String userUID;
    @NotBlank(message = "Campo Obrigatório")
    public String email;
    @NotBlank(message = "Campo Obrigatório")
    public String displayName;
    @NotBlank(message = "Campo Obrigatório")
    public Double qtdStar;
    @NotBlank(message = "Campo Obrigatório")
    public String commentary;

}
