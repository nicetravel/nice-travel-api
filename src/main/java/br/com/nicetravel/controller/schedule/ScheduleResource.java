package br.com.nicetravel.controller.schedule;

import br.com.nicetravel.controller.scheduleday.ScheduleDayResource;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ScheduleResource {

    @NotBlank(message = "Campo Obrigatório")
    public String placeID;
    @NotBlank(message = "Campo Obrigatório")
    public String userUID;
    @NotBlank(message = "Campo Obrigatório")
    public String userEmail;
    @NotBlank(message = "Campo Obrigatório")
    public String userName;


    @NotNull
    public List<ScheduleDayResource> scheduleDays;

    public Integer getNumberDays() {
        return scheduleDays.size();
    }

}
