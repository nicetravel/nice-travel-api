package br.com.nicetravel.controller.schedule;

import java.util.List;

public class ScheduleDayPublishResponse {

    private final Long scheduleDayId;
    private final Integer day;
    private final List<ActivityPublishResponse> activities;

    public ScheduleDayPublishResponse(Long scheduleDayId, Integer day,
                                      List<ActivityPublishResponse> activities) {
        this.scheduleDayId = scheduleDayId;
        this.day = day;
        this.activities = activities;
    }

    public Long getScheduleDayId() {
        return scheduleDayId;
    }

    public Integer getDay() {
        return day;
    }

    public List<ActivityPublishResponse> getActivities() {
        return activities;
    }
}
