package br.com.nicetravel.controller.schedule;

import br.com.nicetravel.service.travel.persist.AbstractPublishTravelScheduleService;
import br.com.nicetravel.service.travel.persist.AbstractTravelScheduleService;
import br.com.nicetravel.service.travel.persist.AbstractVoteTravelScheduleService;
import br.com.nicetravel.service.travel.retrieve.AbstractFindTravelScheduleService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/schedules")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Schedule")
@SecurityScheme(securitySchemeName = "Basic Auth", type = SecuritySchemeType.HTTP, scheme = "basic")
public class ScheduleController {

    private final AbstractTravelScheduleService travelScheduleService;
    private final AbstractPublishTravelScheduleService publishTravelScheduleService;
    private final AbstractVoteTravelScheduleService voteTravelScheduleService;
    private final AbstractFindTravelScheduleService findTravelScheduleService;

    public ScheduleController(AbstractTravelScheduleService travelScheduleService,
                              AbstractPublishTravelScheduleService publishTravelScheduleService,
                              AbstractFindTravelScheduleService findTravelScheduleService,
                              AbstractVoteTravelScheduleService voteTravelScheduleService) {
        this.travelScheduleService = travelScheduleService;
        this.publishTravelScheduleService = publishTravelScheduleService;
        this.findTravelScheduleService = findTravelScheduleService;
        this.voteTravelScheduleService = voteTravelScheduleService;
    }

    @GET
    @Path("/city")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Return list schedules",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Return list schedules according parameters")
    @RolesAllowed("user")
    public List<ScheduleResponse> getSchedulesByCity(@QueryParam(value = "placeID") String placeID,
                                                     @DefaultValue("10") @QueryParam(value = "sizeElements") Integer sizeElements) {
        return findTravelScheduleService.getScheduleByPlaceID(placeID, sizeElements);
    }

    @GET
    @Path("/user/{userUID}")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Return list schedules by user ID",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Return list schedules by user ID")
    @RolesAllowed("user")
    public List<ScheduleResponse> getSchedulesByUserUID(@PathParam("userUID") @Valid @NotNull String userUID) {
        return findTravelScheduleService.retrieveTravelScheduleByUserUID(userUID);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @APIResponses(value = {
            @APIResponse(responseCode = "201", description = "Create Schedule Travel",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Return Schedule Travel created")
    @RolesAllowed("user")
    public Response createTravelSchedule(@Valid ScheduleResource schedule) {
        return Response.ok(publishTravelScheduleService.generateTravelSchedule(schedule)).status(201).build();
    }

    @DELETE
    @Path("/{scheduleId}")
    @Operation(summary = "Delete Schedule")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Delete the travel Schedule",
                    content = @Content(mediaType = "application/json")),
    })
    @RolesAllowed("user")
    public Response deleteTravelSchedule(@PathParam("scheduleId") @Valid @NotNull Long scheduleId) {
        travelScheduleService.logicalDelete(scheduleId);
        return Response.status(204).build();
    }

    @POST
    @Path("/republish/{scheduleId}")
    @APIResponses(value = {
            @APIResponse(responseCode = "201", description = "Return new Schedule with updates.",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Republish Schedule")
    @RolesAllowed("user")
    public Response rePublishTravelSchedule(@PathParam("scheduleId") Long scheduleId, @Valid ScheduleResource schedule) {
        return Response.ok(publishTravelScheduleService.rePublishTravelSchedule(scheduleId, schedule)).status(201).build();
    }

    @POST
    @Path("/duplicate")
    @APIResponses(value = {
            @APIResponse(responseCode = "201", description = "Return schedule duplicated",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Duplicate Schedule to new User")
    @RolesAllowed("user")
    public Response duplicateSchedule(@Valid DuplicateScheduleResource duplicateSchedule) {
        DuplicatedScheduleResponse scheduleResponse = travelScheduleService.duplicateSchedule(duplicateSchedule.scheduleId,
                duplicateSchedule.userUID,
                duplicateSchedule.userEmail,
                duplicateSchedule.userName);
        return Response.accepted(scheduleResponse).status(201).build();
    }

    @PATCH
    @Path("/vote")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Return true if vote is computed",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Vote in Schedule")
    @RolesAllowed("user")
    public boolean voteTravelSchedule(VoteScheduleResource voteSchedule) {
        return voteTravelScheduleService.voteTravelSchedule(voteSchedule.scheduleId,
                voteSchedule.userUID,
                voteSchedule.email,
                voteSchedule.displayName,
                voteSchedule.positiveVote);
    }

    @PATCH
    @Path("/commentary")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Returns true if is new vote",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Include Commentary in Schedule")
    @RolesAllowed("user")
    public boolean commentaryTravelSchedule(CommentaryScheduleResource commentaryResource) {
       return voteTravelScheduleService.commentaryTravelSchedule(commentaryResource.scheduleId,
                commentaryResource.userUID,
                commentaryResource.email,
                commentaryResource.displayName,
                commentaryResource.commentary,
                commentaryResource.qtdStar);
    }

    @GET
    @Path("/commentaries")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Return list commentaries",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Return list commentaries according parameters")
    @RolesAllowed("user")
    public List<RaitingResponse> findAllCommentaries(@QueryParam(value = "scheduleId") Long scheduleId,
                                                     @DefaultValue("10") @QueryParam(value = "sizeElements") Integer sizeElements) {
        return voteTravelScheduleService.findCommentaries(scheduleId, sizeElements);
    }

}
