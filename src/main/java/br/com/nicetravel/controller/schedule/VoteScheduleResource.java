package br.com.nicetravel.controller.schedule;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class VoteScheduleResource {

    @NotNull(message = "Campo Obrigatório")
    public Long scheduleId;
    @NotBlank(message = "Campo Obrigatório")
    public String userUID;
    @NotBlank(message = "Campo Obrigatório")
    public String displayName;
    @NotBlank(message = "Campo Obrigatório")
    public String email;
    public Boolean positiveVote = true;

}
