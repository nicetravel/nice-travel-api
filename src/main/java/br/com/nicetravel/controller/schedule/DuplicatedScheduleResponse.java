package br.com.nicetravel.controller.schedule;

import br.com.nicetravel.controller.scheduleday.ScheduleDayDuplicatedResponse;

import java.util.List;

public class DuplicatedScheduleResponse {

    private final ScheduleResponse cronograma;
    private final List<ScheduleDayDuplicatedResponse> diasCronogramas;

    public DuplicatedScheduleResponse(ScheduleResponse cronograma,
                                      List<ScheduleDayDuplicatedResponse> diasCronogramas) {
        this.cronograma = cronograma;
        this.diasCronogramas = diasCronogramas;
    }

    public ScheduleResponse getCronograma() {
        return cronograma;
    }

    public List<ScheduleDayDuplicatedResponse> getDiasCronogramas() {
        return diasCronogramas;
    }
}
