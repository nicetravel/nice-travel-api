package br.com.nicetravel.controller.schedule;

import br.com.nicetravel.model.ScheduleTravelEntity;

import java.math.BigDecimal;
import java.util.List;

public class ScheduleResponse {

    private final int qtdDays;
    private final List<String> imagesUrl;
    private final String cityAddress;
    private final Long scheduleCod;
    private final BigDecimal priceFinal;
    private final String userUID;
    private final String userEmail;
    private final String placeID;
    private final String userName;
    private final Integer numberLike;
    private final Boolean publish;

    public ScheduleResponse(ScheduleTravelEntity scheduleTravel) {
        this.priceFinal = scheduleTravel.getPriceFinal();
        this.scheduleCod = scheduleTravel.getCod();
        this.imagesUrl = scheduleTravel.getCityImageUrl(5);
        this.cityAddress = scheduleTravel.getFormattedAdress();
        this.qtdDays = scheduleTravel.getScheduleDayEntities().size();
        this.userName = scheduleTravel.getUserOwner().getName();
        this.userUID = scheduleTravel.getUserOwner().getUid();
        this.userEmail = scheduleTravel.getUserOwner().getEmail();
        this.placeID = scheduleTravel.getCityEntity().getPlaceID();
        this.numberLike = scheduleTravel.getNumberLike();
        this.publish = scheduleTravel.getPublicAccess();
    }

    public ScheduleResponse(int qtdDays, List<String> imagesUrl, String cityAddress,
                            Long scheduleCod, BigDecimal priceFinal, String userUID,
                            String userName, Integer numberLike, Boolean publish,
                            String userEmail, String placeID) {
        this.qtdDays = qtdDays;
        this.imagesUrl = imagesUrl;
        this.cityAddress = cityAddress;
        this.scheduleCod = scheduleCod;
        this.priceFinal = priceFinal;
        this.userUID = userUID;
        this.userName = userName;
        this.numberLike = numberLike;
        this.publish = publish;
        this.userEmail = userEmail;
        this.placeID = placeID;
    }

    public int getQtdDays() {
        return qtdDays;
    }

    public List<String> getImagesUrl() {
        return imagesUrl;
    }

    public String getCityAddress() {
        return cityAddress;
    }

    public Long getScheduleCod() {
        return scheduleCod;
    }

    public BigDecimal getPriceFinal() {
        return priceFinal;
    }

    public String getUserUID() {
        return userUID;
    }

    public String getUserName() {
        return userName;
    }

    public Integer getNumberLike() {
        return numberLike;
    }

    public Boolean getPublish() {
        return publish;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getPlaceID() {
        return placeID;
    }

}
