package br.com.nicetravel.controller.schedule;

import br.com.nicetravel.model.VoteScheduleEntity;

import java.time.LocalDate;

public class RaitingResponse {

    private final Double qtdStar;
    private final String description;
    private final String userUid;
    private final LocalDate date;

    public RaitingResponse(VoteScheduleEntity s) {
        this.qtdStar = s.getQtdStart();
        this.description = s.getCommentary();
        this.date = s.getDtVote();
        this.userUid = s.getUserVote().getUid();
    }

    public Double getQtdStar() {
        return qtdStar;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getUserUid() {
        return userUid;
    }
}
