insert into DB_NICE_TRAVEL.TB_USER (CO_USER, DS_EMAIL, DS_NAME, CO_UID_FIREBASE)
values (1, 'thiago@gmail.com', 'thiago', '1');

insert into DB_NICE_TRAVEL.TB_USER (CO_USER, DS_EMAIL, DS_NAME, CO_UID_FIREBASE)
values (2, 'jose@gmail.com', 'jose', '2');

insert into DB_NICE_TRAVEL.TB_CITY (CO_CITY, FORMATTED_ADDRESS, DS_LATITUDE, DS_LONGITUDE, DS_NAME, CO_PLACE_ID)
VALUES (1, 'Salvador - BA', 12, 123, 'Salvador', '1');

insert into DB_NICE_TRAVEL.TB_CITY (CO_CITY, FORMATTED_ADDRESS, DS_LATITUDE, DS_LONGITUDE, DS_NAME, CO_PLACE_ID)
VALUES (2, 'Porto Seguro - BA', 12, 123, 'Porto Seguro', 'ChIJIb5p4kuSqgcRUiZizPUS4TQ');

INSERT INTO DB_NICE_TRAVEL.TB_PHOTO_CITY (CO_PHOTO_CITY, TX_PHOTO, CO_CITY) VALUES (1, 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=1125&photoreference=ATtYBwKkZdr0p32Lr8FILTxvkJFH8nfPesHn3a7YSCgyqIJngVXxY_vkdwiyFxHj8alVu6eufiltO5oG4vyDy5WcQQ98DXIhiFJMPROSDOeKAuVDTj5KalJrn_FHJJOBzSUr4ENPHVx6d9YnhvO7ON4PoPDvY2Z5eVRcuDmqKFIAZ_gcTzbR&key=AIzaSyCu52jF0FrKZoj2AgMlSDTvCU6pS1t3KU8', 1);
INSERT INTO DB_NICE_TRAVEL.TB_PHOTO_CITY (CO_PHOTO_CITY, TX_PHOTO, CO_CITY) VALUES (2, 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=1125&photoreference=ATtYBwKkZdr0p32Lr8FILTxvkJFH8nfPesHn3a7YSCgyqIJngVXxY_vkdwiyFxHj8alVu6eufiltO5oG4vyDy5WcQQ98DXIhiFJMPROSDOeKAuVDTj5KalJrn_FHJJOBzSUr4ENPHVx6d9YnhvO7ON4PoPDvY2Z5eVRcuDmqKFIAZ_gcTzbR&key=AIzaSyCu52jF0FrKZoj2AgMlSDTvCU6pS1t3KU8', 2);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_TRAVEL (CO_SCHEDULE_TRAVEL, nu_like, ST_PUBLIC_ACCESS, CO_CITY, CO_USER)
values (99, 1, true, 1, 2);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_TRAVEL (CO_SCHEDULE_TRAVEL, nu_like, ST_PUBLIC_ACCESS, CO_CITY, CO_USER)
values (98, 1, true, 2, 1);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_TRAVEL (CO_SCHEDULE_TRAVEL, nu_like, ST_PUBLIC_ACCESS, CO_CITY, CO_USER)
values (97, 1, true, 2, 1);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_DAY (CO_SCHEDULE_DAY, NU_DAY, CO_SCHEDULE_TRAVEL)
VALUES (99, 1, 99);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_DAY (CO_SCHEDULE_DAY, NU_DAY, CO_SCHEDULE_TRAVEL)
VALUES (98, 1, 99);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_DAY (CO_SCHEDULE_DAY, NU_DAY, CO_SCHEDULE_TRAVEL)
VALUES (97, 1, 98);

insert into DB_NICE_TRAVEL.TB_SCHEDULE_DAY (CO_SCHEDULE_DAY, NU_DAY, CO_SCHEDULE_TRAVEL)
VALUES (96, 1, 98);

insert into DB_NICE_TRAVEL.TB_ACTIVITY (CO_ACTIVITY, DS_DESCRIBE, DT_END, DT_START, DS_NAME, VL_PRICE, DS_TYPE_ACTIVITY,
                                        CO_SCHEDULE_DAY)
values (96, 'Corrida na praia', now(), now(), 'Corrida na praia', 10.0, 'PARK', 96);

insert into DB_NICE_TRAVEL.TB_ACTIVITY (CO_ACTIVITY, DS_DESCRIBE, DT_END, DT_START, DS_NAME, VL_PRICE, DS_TYPE_ACTIVITY,
                                        CO_SCHEDULE_DAY)
values (97, 'Café da manhã', now(), now(), 'Café da manhã', 20.0, 'RESTAURANT', 97);

insert into DB_NICE_TRAVEL.TB_ACTIVITY (CO_ACTIVITY, DS_DESCRIBE, DT_END, DT_START, DS_NAME, VL_PRICE, DS_TYPE_ACTIVITY,
                                        CO_SCHEDULE_DAY)
values (98, 'Corrida', now(), now(), 'Parque', 10.0, 'PARK', 99);

insert into DB_NICE_TRAVEL.TB_ACTIVITY (CO_ACTIVITY, DS_DESCRIBE, DT_END, DT_START, DS_NAME, VL_PRICE, DS_TYPE_ACTIVITY,
                                        CO_SCHEDULE_DAY)
values (99, 'Almoço do João', now(), now(), 'Almoço do parque', 20.0, 'RESTAURANT', 99);

