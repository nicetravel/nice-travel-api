package br.com.nicetravel.controller;

import br.com.nicetravel.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestProfile(MockProfile.class)
class ActivityControllerTest {

    @Test
    void testGetActivities() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .get("/activities/99")
                .then()
                .statusCode(200)
                .body(
                        "$.size()", is(2),
                        "nameOfPlace", containsInAnyOrder("Parque", "Almoço do parque"),
                        "description", containsInAnyOrder("Almoço do João", "Corrida"),
                        "styleActivity", containsInAnyOrder("Park", "Restaurant")
                );

    }

    @Test
    void shouldDeleteActivity() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .delete("/activities/97")
                .then()
                .statusCode(204);
    }


    @Test
    void shouldCreateActivity() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .body("{\n" +
                        "    \"description\" : \"Corrida\",\n" +
                        "    \"nameOfPlace\" : \"Parque da Cidade\",\n" +
                        "    \"price\" : 10.50,\n" +
                        "    \"startActivity\" : \"12:00:00.000\",\n" +
                        "    \"finishActivity\" : \"13:00:00.000\",\n" +
                        "    \"styleActivity\" : \"PARK\",\n" +
                        "    \"idScheduleDay\" : 98\n" +
                        "}")
                .post("/activities")
                .then()
                .statusCode(201)
                .body(
                        "startActivity", equalTo(Arrays.asList(12, 0)),
                        "finishActivity", equalTo(Arrays.asList(13, 0)),
                        "nameOfPlace", equalTo("Parque da Cidade"),
                        "description", equalTo("Corrida"),
                        "price", equalTo(10.5F),
                        "idScheduleDay", equalTo(98),
                        "id", equalTo(1)
                );
    }
}
