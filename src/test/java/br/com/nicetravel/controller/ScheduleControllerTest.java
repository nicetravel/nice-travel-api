package br.com.nicetravel.controller;

import br.com.nicetravel.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;

@QuarkusTest
@TestProfile(MockProfile.class)
class ScheduleControllerTest {

    @Test
    void shouldGetSchedule() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .param("sizeElements", 1)
                .param("placeID", "1")
                .get("/schedules/city")
                .then()
                .statusCode(200)
                .body(
                        "$.size()", is(1),
                        "qtdDays", containsInAnyOrder(2),
                        "cityAddress", containsInAnyOrder("Salvador - BA"),
                        "scheduleCod", containsInAnyOrder(99)
                );
    }

    @Test
    void shouldCreateScheduleTravel() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .body("{\n" +
                        "   \"placeID\":\"ChIJIb5p4kuSqgcRUiZizPUS4TQ\",\n" +
                        "   \"userEmail\":\"thiago@gmail.com\",\n" +
                        "   \"userName\":\"Thiago\",\n" +
                        "   \"userUID\":\"1\",\n" +
                        "   \"scheduleDays\":[\n" +
                        "      {\n" +
                        "         \"day\":1,\n" +
                        "         \"id\":null,\n" +
                        "         \"activities\":[\n" +
                        "            \n" +
                        "         ]\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"day\":2,\n" +
                        "         \"id\":null,\n" +
                        "         \"activities\":[\n" +
                        "            \n" +
                        "         ]\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"day\":3,\n" +
                        "         \"id\":null,\n" +
                        "         \"activities\":[\n" +
                        "            \n" +
                        "         ]\n" +
                        "      }\n" +
                        "   ]\n" +
                        "}")
                .post("/schedules")
                .then()
                .statusCode(201)
                .body(
                        "imagesUrl.size()", equalTo(1),
                        "scheduleDays.size()", is(3),
                        "scheduleCod", in(List.of(1,2))
                );
    }

    @Test
    void shouldGetSchedulesByUserUID() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .get("/schedules/user/2")
                .then()
                .statusCode(200)
                .body(
                        "$.size()", is(1),
                        "qtdDays", containsInAnyOrder(2),
                        "cityAddress", containsInAnyOrder("Salvador - BA"),
                        "scheduleCod", containsInAnyOrder(99)
                );
    }

    @Test
    void shouldDeleteTravelSchedule() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .delete("/schedules/97")
                .then()
                .statusCode(204);
    }

    @Test
    void shouldRePublishTravelSchedule() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .body("{\n" +
                        "   \"placeID\":\"ChIJIb5p4kuSqgcRUiZizPUS4TQ\",\n" +
                        "   \"userEmail\":\"thiago@gmail.com\",\n" +
                        "   \"userName\":\"Thiago\",\n" +
                        "   \"userUID\":\"1\",\n" +
                        "   \"scheduleDays\":[\n" +
                        "      {\n" +
                        "         \"day\":1,\n" +
                        "         \"id\":null,\n" +
                        "         \"activities\":[\n" +
                        "            \n" +
                        "         ]\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"day\":2,\n" +
                        "         \"id\":null,\n" +
                        "         \"activities\":[\n" +
                        "            \n" +
                        "         ]\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"day\":3,\n" +
                        "         \"id\":null,\n" +
                        "         \"activities\":[\n" +
                        "            \n" +
                        "         ]\n" +
                        "      }\n" +
                        "   ]\n" +
                        "}")
                .post("/schedules/republish/98")
                .then()
                .statusCode(201)
                .body(
                        "imagesUrl.size()", equalTo(1),
                        "scheduleDays.size()", is(3),
                        "scheduleCod", not(98)
                );
    }

    @Test
    void shouldDuplicateSchedule() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .body("{\n" +
                        "    \"userName\" : \"Thiago\",\n" +
                        "    \"userEmail\" : \"thiago@gmail.com\",\n" +
                        "    \"scheduleId\" : \"99\",\n" +
                        "    \"userUID\" : \"1\"\n" +
                        "}")
                .contentType("application/json")
                .post("/schedules/duplicate")
                .then()
                .statusCode(201)
                .body(
                        "cronograma.qtdDays", equalTo(2),
                        "cronograma.cityAddress", equalTo("Salvador - BA"),
                        "cronograma.userName", equalTo("Thiago")
                );
    }

    @Test
    void shouldVoteTravelSchedule() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .body("{\n" +
                        "    \"userUID\" : \"2\",\n" +
                        "    \"scheduleId\" : \"99\",\n" +
                        "    \"positiveVote\" : true\n" +
                        "}")
                .patch("/schedules/vote")
                .then()
                .statusCode(200)
                .body(equalTo("true"));
    }
}
