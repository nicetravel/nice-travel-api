package br.com.nicetravel.controller;

import br.com.nicetravel.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestProfile(MockProfile.class)
class ScheduleDayControllerTest {

    @Test
    void shouldGetScheduleDaysByScheduleCod() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .get("/schedulesDay/98")
                .then()
                .statusCode(200)
                .body(
                        "$.size()", is(2),
                        "qtdActivities", containsInAnyOrder(1, 0)
                );
    }

    @Test
    void shouldAddScheduleDay() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .post("/schedulesDay/98")
                .then()
                .statusCode(201)
                .body(
                        "day", equalTo(3),
                        "qtdActivities", equalTo(0)
                );
    }

    @Test
    void shouldDeleteByScheduleDayId() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .delete("/schedulesDay/99")
                .then()
                .statusCode(204);
    }

    @Test
    void shouldNotFoundWhenDeleteByScheduleDayNotExists() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .delete("/schedulesDay/105")
                .then()
                .statusCode(404);
    }

    @Test
    void shouldReorderScheduleDays() {
        given()
                .when()
                .auth().preemptive().basic("admin", "test")
                .contentType("application/json")
                .body("{\n" +
                        "    \"scheduleDayIdFrom\" : 96,\n" +
                        "    \"scheduleDayIdTo\" : 97\n" +
                        "}")
                .post("/schedulesDay/reorder")
                .then()
                .statusCode(204);
    }

}
