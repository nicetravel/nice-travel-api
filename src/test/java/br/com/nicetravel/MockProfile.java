package br.com.nicetravel;

import io.quarkus.test.junit.QuarkusTestProfile;

import java.util.Map;

public class MockProfile implements QuarkusTestProfile {

    @Override
    public Map<String, String> getConfigOverrides() {
        return Map.of(
                "quarkus.datasource.jdbc.url", "jdbc:h2:mem:nicetravel;INIT=CREATE SCHEMA IF NOT EXISTS DB_NICE_TRAVEL;MODE=POSTGRESQL",
                "quarkus.datasource.username", "org.h2.Driver",
                "quarkus.datasource.password", "drop-and-create",
                "quarkus.datasource.db-kind", "h2",
                "quarkus.hibernate-orm.database.generation", "drop-and-create",
                "quarkus.hibernate-orm.sql-load-script", "database/init.sql",
                "quarkus.security.users.embedded.users.admin", "test",
                "nice-travel.google.api-key", "test"
        );
    }
}
