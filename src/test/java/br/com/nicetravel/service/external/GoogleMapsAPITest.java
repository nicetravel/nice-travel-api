package br.com.nicetravel.service.external;

import br.com.nicetravel.exceptions.GooglePlaceNotFoundException;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertThrows;

@QuarkusTest
@Disabled("This test call Google Place API")
class GoogleMapsAPITest {

    @Autowired
    private GoogleMapsAPI googleMapsAPI;

    @Test
    public void testGetTypeLocation() {
        PlaceDTO placeDTO = googleMapsAPI.getPlaceDTO("ChIJIb5p4kuSqgcRUiZizPUS4TQ");
        Assertions.assertEquals("natural_feature,establishment", placeDTO.getTypes());
        Assertions.assertEquals(new Double(-8.4844765), placeDTO.getLat());
        Assertions.assertEquals(new Double(-34.9996923), placeDTO.getLng());
        Assertions.assertEquals("Porto de Galinhas", placeDTO.getName());
    }

    @Test
    public void shouldNotSaveCity() {
        Throwable exception = assertThrows(GooglePlaceNotFoundException.class,
                () -> googleMapsAPI.getPlaceDTO("FAKE_PLACE_ID"));

        Assertions.assertEquals("Houve um problema de integração: NOT FOUND A CITY WITH PLACE ID FAKE_PLACE_ID", exception.getMessage());
    }

}
